
function CreateUser(firstName, lastName, birthday){
	this.firstName=firstName;
	this.lastName=lastName;
	this.birthday=birthday;
};

CreateUser.prototype.ask=function(){
	this.firstName=prompt("Add your first name");
	this.lastName=prompt("Add your last name");
	this.birthday=prompt("Add your birthday in format dd.mm.yyyy");
	return this.firstName, this.lastName, this.birthday;
}
CreateUser.prototype.getAge=function(){
	let dateb=this.birthday.split(".");
	let birthDate=new Date (`${dateb[2]}-${dateb[1]}-${dateb[0]}`);
	let age=((new Date().getTime() - new Date(birthDate))/ (24 * 3600 * 365.25 * 1000)) | 0;;
	return age;
}
CreateUser.prototype.getLogin=function(){
	let login=this.firstName.charAt(0).toLocaleLowerCase()+this.lastName.toLocaleLowerCase();
	return login;};
CreateUser.prototype.getPassword=function(){
	let dated=this.birthday.split(".");
	let yearBirth=dated[2];
	let password=(this.firstName.charAt(0)+this.lastName.toLocaleLowerCase()+yearBirth);
	return password;
};

let newUser = new CreateUser();
newUser.ask();
newUser.getLogin();
newUser.getAge();
newUser.getPassword();

document.getElementById("main").innerHTML=`Users name: ${newUser.ask()} <br> Users age: ${newUser.getAge()} <br> Users login: ${newUser.getLogin()} <br> Users password: ${newUser.getPassword()}`;


function filtrBy (arr, dataType){
	let result = arr.filter(arr => typeof arr === dataType);
	return result;
}
let a=["man", 21, "woman", 53, "child", 15];
let b = 'number';
let d= filtrBy(a, b)
document.getElementById("main").innerHTML=`Array: ${a.join(", ")} <br> Array filterd by number: ${d}`;
