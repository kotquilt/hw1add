//Створи клас, який буде створювати користувачів з ім'ям та прізвищем. //Додати до класу метод для виведення імені та прізвища
class User{
	constructor(name, lastName){
		this.name=name;
		this.lastName=lastName;
	}
	show(){
		this.User=document.createElement("div");
		document.body.append(this.User);
		this.User.style.cssText=
		`width:300px;
		font: 12px;
		color: green;
		border: 2px solid black;
		borderradius: 10px;`
		this.User.textContent=`Name: ${this.name}. Last Name: ${this.lastName}. `
	}
}
let user1=new User;
user1.name="Kot";
user1.lastName="Red"
user1.show()
//Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний
const ul =document.createElement ("ul"),
li1=document.createElement("li"),
li2=document.createElement("li"),
li3=document.createElement("li"),
li4=document.createElement("li");
document.body.append(ul);
ul.append(li1, li2, li3,li4);

let a=document.getElementsByTagName("li")[2];
a.previousElementSibling.style.color="blue";
a.nextElementSibling.style.color="red";

//Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

const b=document.createElement("div");
document.body.append(b);
b.style.height="400px";
b.onmousemove=function (e){
	this.innerHTML="X:"+ e.offsetX+ "Y:"+ e.offsetY;
}

//Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута

const buttons =document.createElement("div"),
button1 = document.createElement("button"),
button2 = document.createElement("button"),
button3 = document.createElement("button"),
button4 = document.createElement("button");
document.body.append(buttons);
buttons.append(button1, button2, button3, button4);

button1.innerText="1";
button2.innerText="2";
button3.innerText="3";
button4.innerText="4";
buttons.addEventListener("click", (e) => {
	const a = document.createElement("div");
	buttons.append(a);
	a.innerText=e.target.innerText;
});
// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
const div = document.createElement ("div");
document.body.append(div);
div.style.height = "100px";
div.style.width = "100px";
div.style.backgroundColor = "#10ff01";
div.style.border="2px solid black";
div.style.transform = "translateY(10px)";
div.style.transition = "2s"

const mouse = (e) =>{
		e.target.style.transform = "translateY(100px)";
};

div.addEventListener("mouseover", mouse);
div.onmouseout=function(e){
    e.target.style.transform = "translateY(10px)"
};

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body

const div2 = document.createElement ("div");
document.body.append(div2);
div2.style.marginTop= "50px";
const colorLabel= document.createElement("label"),
colorInput=document.createElement("input"),
choose=document.createElement("button");
div2.append(colorLabel, colorInput, choose);
colorLabel.textContent="Choose color";
colorInput.style.width="100px";
colorInput.setAttribute("type", "color");
choose.textContent="Choose"
choose.addEventListener("click", () =>{
	let color=colorInput.value;
	document.body.style.backgroundColor=color;
})

// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль
const div3=document.createElement("div"),
login=document.createElement("input");
document.body.append(div3);
div3.append(login);
div3.style.marginTop="50px";
login.setAttribute("placeholder", "Add Login")

login.addEventListener("keypress", () =>{
	console.log(login.value)
});


// Створіть поле для введення даних у полі введення даних виведіть текст під полем

const div4=document.createElement("div"), 
data=document.createElement("input"),
data1=document.createElement("p");
document.body.append(div4);
div4.append(data);
div4.append(data1);
div4.style.marginTop="50px";
data.setAttribute("placeholder", "Enter something");
data.addEventListener("input", ()=>{
	data1.textContent=data.value
});
