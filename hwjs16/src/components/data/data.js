const data = [
	{ 
	"r030":36,"txt":"Австралійський долар","rate":25.918,"cc":"AUD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":124,"txt":"Канадський долар","rate":28.6027,"cc":"CAD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":156,"txt":"Юань Женьміньбі","rate":5.4238,"cc":"CNY","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":191,"txt":"Куна","rate":5.0034,"cc":"HRK","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":203,"txt":"Чеська крона","rate":1.542,"cc":"CZK","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":208,"txt":"Данська крона","rate":5.0535,"cc":"DKK","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":344,"txt":"Гонконгівський долар","rate":4.6669,"cc":"HKD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":348,"txt":"Форинт","rate":0.095842,"cc":"HUF","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":356,"txt":"Індійська рупія","rate":0.45912,"cc":"INR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":360,"txt":"Рупія","rate":0.0024927,"cc":"IDR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":376,"txt":"Новий ізраїльський шекель","rate":11.2758,"cc":"ILS","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":392,"txt":"Єна","rate":0.27331,"cc":"JPY","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":398,"txt":"Теньге","rate":0.076636,"cc":"KZT","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":410,"txt":"Вона","rate":0.028079,"cc":"KRW","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":484,"txt":"Мексиканське песо","rate":1.8351,"cc":"MXN","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":498,"txt":"Молдовський лей","rate":1.8971,"cc":"MDL","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":554,"txt":"Новозеландський долар","rate":23.5063,"cc":"NZD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":578,"txt":"Норвезька крона","rate":3.83,"cc":"NOK","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":643,"txt":"Російський рубль","rate":0.59736,"cc":"RUB","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":702,"txt":"Сінгапурський долар","rate":26.6574,"cc":"SGD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":710,"txt":"Ренд","rate":2.2487,"cc":"ZAR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":752,"txt":"Шведська крона","rate":3.597,"cc":"SEK","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":756,"txt":"Швейцарський франк","rate":38.8037,"cc":"CHF","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":818,"txt":"Єгипетський фунт","rate":1.9094,"cc":"EGP","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":826,"txt":"Фунт стерлінгів","rate":44.3669,"cc":"GBP","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":840,"txt":"Долар США","rate":36.5686,"cc":"USD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":933,"txt":"Білоруський рубль","rate":13.2919,"cc":"BYN","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":944,"txt":"Азербайджанський манат","rate":21.5706,"cc":"AZN","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":946,"txt":"Румунський лей","rate":7.6852,"cc":"RON","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":949,"txt":"Турецька ліра","rate":2.0359,"cc":"TRY","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":960,"txt":"СПЗ (спеціальні права запозичення)","rate":48.4206,"cc":"XDR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":975,"txt":"Болгарський лев","rate":19.2234,"cc":"BGN","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":978,"txt":"Євро","rate":37.5943,"cc":"EUR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":985,"txt":"Злотий","rate":8.039,"cc":"PLN","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":12,"txt":"Алжирський динар","rate":0.25091,"cc":"DZD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":50,"txt":"Така","rate":0.38615,"cc":"BDT","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":51,"txt":"Вірменський драм","rate":0.089693,"cc":"AMD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":214,"txt":"Домініканське песо","rate":0.67246,"cc":"DOP","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":364,"txt":"Іранський ріал","rate":0.00087068,"cc":"IRR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":368,"txt":"Іракський динар","rate":0.025047,"cc":"IQD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":417,"txt":"Сом","rate":0.43966,"cc":"KGS","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":422,"txt":"Ліванський фунт","rate":0.024258,"cc":"LBP","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":434,"txt":"Лівійський динар","rate":7.5137,"cc":"LYD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":458,"txt":"Малайзійський ринггіт","rate":8.2162,"cc":"MYR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":504,"txt":"Марокканський дирхам","rate":3.5527,"cc":"MAD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":586,"txt":"Пакистанська рупія","rate":0.15316,"cc":"PKR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":682,"txt":"Саудівський ріял","rate":9.7363,"cc":"SAR","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":704,"txt":"Донг","rate":0.0015659,"cc":"VND","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":764,"txt":"Бат","rate":1.00882,"cc":"THB","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":784,"txt":"Дирхам ОАЕ","rate":9.9558,"cc":"AED","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":788,"txt":"Туніський динар","rate":11.6305,"cc":"TND","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":860,"txt":"Узбецький сум","rate":0.0033511,"cc":"UZS","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":901,"txt":"Новий тайванський долар","rate":1.22234,"cc":"TWD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":934,"txt":"Туркменський новий манат","rate":10.4482,"cc":"TMT","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":941,"txt":"Сербський динар","rate":0.31815,"cc":"RSD","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":972,"txt":"Сомоні","rate":3.562,"cc":"TJS","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":981,"txt":"Ларі","rate":13.2615,"cc":"GEL","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":986,"txt":"Бразильський реал","rate":7.056,"cc":"BRL","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":959,"txt":"Золото","rate":65315.91,"cc":"XAU","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":961,"txt":"Срібло","rate":742.94,"cc":"XAG","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":962,"txt":"Платина","rate":34791,"cc":"XPT","exchangedate":"15.08.2022"
	 }
	,{ 
	"r030":964,"txt":"Паладій","rate":82092.85,"cc":"XPD","exchangedate":"15.08.2022"
	 }
	]

export default data
