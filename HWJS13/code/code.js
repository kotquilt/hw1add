let url = "https://swapi.dev/api/people";
let allCards = [];
localStorage.user = JSON.stringify([]);

class Cards {
	constructor(name, gender, height, skin_color, birth_year, homeworld) {
		this.name = name;
		this.gender = gender;
		this.height = height;
		this.skin_color = skin_color;
		this.birth_year = birth_year;
		this.homeworld = homeworld;

	}
	show() {
		let card = document.createElement("div");
		card.classList.add("card");
		document.querySelector(".sw").append(card);
		
		let cardName = document.createElement("h1"),
			cardGender = document.createElement("p"),
			cardHeight = document.createElement("p"),
			cardSkin = document.createElement("p"),
			cardBirth = document.createElement("p"),
			cardHome = document.createElement("p"),
			button = document.createElement("button");
		cardName.classList.add("h1");
		card.append(cardName, cardGender, cardHeight, cardSkin, cardBirth, cardHome, button);

		button.textContent = "Save";

		cardName.textContent = `Character name: ${this.name}`;
		cardGender.textContent = `Gender: ${this.gender}`;
		cardHeight.textContent = `Height: ${this.height}`;
		cardSkin.textContent = `Skin color: ${this.skin_color}`;
		cardBirth.textContent = `Year of birth: ${this.birth_year}`;
		cardHome.textContent = `Home planet: ${this.homeworld}`;

	}
	load() {
		document.querySelector("button").addEventListener("click", (e) => {
			let a = JSON.parse(localStorage.user);

				a.push(	new Cards)

				localStorage.user=JSON.stringify(a);
			}
		
		);
	}
}


	const data = fetch(url);
let rez = data.then((a) => a.json(), (error) => console.error(error))

rez.then((people) => {
	people.results.forEach((element) => {
		let person = new Cards(
			element.name,
			element.gender,
			element.height,
			element.skin_color,
			element.birth_year,
			element.homeworld
		);
		person.show();
		person.load();

	});

});





