import React from "react";
import ReactDOM from "react-dom";

function MonthList (props){
	const month = props.month;
	const listMonth = month.map((number, index) =>
		<li key ={index}>{number}</li>
	);
	return (
		<ul>{listMonth}</ul>
	);
};
function WeekDayList (props){
	const days = props.days;
	const listDays = days.map((number, index) =>
		<li key ={index}>{number}</li>
	);
	return (
		<ul>{listDays}</ul>
		
	);
};
function ZodiacList (props){
	const zodiac = props.zodiac;
	const listZodiac = zodiac.map((number, index) =>
		<li key ={index}>{number}</li>
	);
	return (
		<ul>{listZodiac}</ul>
		
	);
};
const App = function () {
	return(
		<div>
		  <h1>Month list</h1>
		   <MonthList month ={month}></MonthList>
		 <h1>Week day list</h1>
		  <WeekDayList days={days}></WeekDayList>
		 <h1>Zodiac list</h1>
		  <ZodiacList zodiac={zodiac}></ZodiacList>  
		</div>
	)
}


const month =["January", "Fabruary", "March", "June", "July", "August", "September", "October", "November", "December"];
const days = ["Monday", "Tuestady", "Wenesday", "Thursday", "Friday", "Saturday", "Sunday"];
const zodiac = ["Capricorn", "Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagitarius"]

ReactDOM.render(<App></App>, document.querySelector("#root"))

